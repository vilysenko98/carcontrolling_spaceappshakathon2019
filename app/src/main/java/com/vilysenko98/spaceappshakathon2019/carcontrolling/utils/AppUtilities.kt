package com.vilysenko98.spaceappshakathon2019.carcontrolling.utils

import android.graphics.Color
import android.graphics.Color.colorToHSV
import java.nio.file.Files.size
import java.util.*


/**
 * Checks if values for array are same as argument
 * @param value Value to compare
 * @return Returns true if values are same as parameter
 */
fun FloatArray.isValuesSame(value: Float): Boolean{
    forEach {
        if(it != value) return false
    }
    return true
}

/**
 * Transforms all radian values in array to degrees
 * @return Copy of variable in degrees
 */
fun FloatArray.radToDeg() =
    FloatArray(size){ i -> Math.toDegrees(this[i].toDouble()).toFloat() }

/**
 * Rounding down to specific amount of decimal digits
 * @param Amount of symbols round down to
 * @return Rounded number
 */
fun Float.roundSymbols(amountOfSymbols: Int): Float{
    val asDouble = this.toDouble()
    val leftover = Math.pow(10.toDouble(), amountOfSymbols.toDouble())
    return (Math.floor(asDouble * leftover) / leftover).toFloat()
}


fun interpolate(a: Float, b: Float, proportion: Float): Float {
    return a + (b - a) * proportion
}

/** Returns an interpoloated color, between `a` and `b`  */
fun interpolateColor(a: Int, b: Int, proportion: Float): Int {
    val hsva = FloatArray(3)
    val hsvb = FloatArray(3)
    colorToHSV(a, hsva)
    colorToHSV(b, hsvb)
    for (i in 0..2) {
        hsvb[i] = interpolate(hsva[i], hsvb[i], proportion)
    }
    return Color.HSVToColor(hsvb)
}

class LimitedQueue<E>(private val limit: Int) : LinkedList<E>() {

    override fun add(o: E): Boolean {
        val added = super.add(o)
        while (added && size > limit) {
            super.remove()
        }
        return added
    }
}

fun LimitedQueue<Float>.average(): Float{
    var sum = 0f
    forEach { sum += it }
    return sum / size
}
