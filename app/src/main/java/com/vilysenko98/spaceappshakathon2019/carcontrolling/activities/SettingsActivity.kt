package com.vilysenko98.spaceappshakathon2019.carcontrolling.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.vilysenko98.spaceappshakathon2019.carcontrolling.R
import com.vilysenko98.spaceappshakathon2019.carcontrolling.utils.*
import kotlinx.android.synthetic.main.activity_settings.*

class SettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        init()
    }

    private fun init(){
        title = "Settings"

        edit_bluetoothAddress.setText(bluetoothDeviceAddress)
        edit_webAddress.setText(webcamWebAddress)
        //Sensor readings converted to degrees
        edit_sensorDeadzone.setText(Math.toDegrees(deadzoneAngle).toFloat().roundSymbols(1).toString())
        edit_sensorMaxAngle.setText(Math.toDegrees(maximumAngle).toFloat().roundSymbols(1).toString())

        check_isControlInverted.isChecked = isControlsInverted
    }

    fun clickApply(v: View){
        bluetoothDeviceAddress = edit_bluetoothAddress.text.toString()
        webcamWebAddress = edit_webAddress.text.toString()
        //Settings for sensor(convert back to radians)
        val deadzone = edit_sensorDeadzone.text.toString().toDoubleOrNull()
        if(deadzone != null)
            deadzoneAngle = Math.toRadians(deadzone)

        val maxAngle = edit_sensorMaxAngle.text.toString().toDoubleOrNull()
        if(maxAngle != null)
            maximumAngle = Math.toRadians(maxAngle)

        isControlsInverted = check_isControlInverted.isChecked

        finish()
    }
}
