package com.vilysenko98.spaceappshakathon2019.carcontrolling.car

import android.util.Log
import com.vilysenko98.spaceappshakathon2019.carcontrolling.utils.LimitedQueue
import com.vilysenko98.spaceappshakathon2019.carcontrolling.utils.average
import com.vilysenko98.spaceappshakathon2019.carcontrolling.utils.deadzoneAngle
import com.vilysenko98.spaceappshakathon2019.carcontrolling.utils.maximumAngle
import java.util.*
import kotlin.math.absoluteValue

class CarValuesProcessor {

    val TAG = "CARVALUEPROECESSOR_TAG"

    //region Variables
    /*//Tilting deadzones
    val deadzoneAngle = Math.toRadians(5.0)
    //Tilting maximum values
    val maximumAngle = Math.toRadians(25.0)*/
    //Tilt readings and calibrations, (indxes: 0 - portrait sides tilt, 1 - landscape side tilt)
    private val tiltAngles = FloatArray(2)
    private val tiltCalibrationAngles = FloatArray(2)
    private var isInitiallyCalibrated = false
    /**
     * Angles with calibration corrections
     */
    val calibratedAngles: FloatArray
        get() = FloatArray(2){
                tiltAngles[it] - tiltCalibrationAngles[it]
            }


    //Car values and callback
    var smoothingRadThreshold = Math.toRadians(0.5)
    var tiltPortraitBuffer = LimitedQueue<Float>(10)
    var tiltLandscapeBuffer = LimitedQueue<Float>(10)

    var speed: Int = 0
        private set(value) { field  = value }
    var rotation: Int = 0
        private set(value) { field  = value }
    val speedPercentage: Float
        get() {
            val speedValue = calibratedAngles[1]
            if (speedValue.absoluteValue > maximumAngle){
                return 1f * (if(speedValue > 0) 1 else -1)
            } else if(speedValue.absoluteValue > deadzoneAngle){
                return ((speedValue.absoluteValue - deadzoneAngle) / (maximumAngle - deadzoneAngle) * (if(speedValue > 0) 1 else -1)).toFloat()
            } else {
                return 0f
            }
        }
    val rotationPercentage: Float
        get() {
            val rotationValue = calibratedAngles[0]
            if (rotationValue.absoluteValue > maximumAngle){
                return 1f * (if(rotationValue > 0) 1 else -1)
            } else if(rotationValue.absoluteValue > deadzoneAngle){
                return ((rotationValue.absoluteValue - deadzoneAngle) / (maximumAngle - deadzoneAngle) * (if(rotationValue > 0) 1 else -1)).toFloat()
            } else {
                return  0f
            }
        }
    private var onCarValuesSet :(speed: Int, rotation: Int) -> Unit = {x, y -> }
    private var onCarValuesChanged :(speed: Int, rotation: Int) -> Unit = {x, y -> }
    //endregion


    //regions Methods
    //Setting up callback listener
    fun setOnCarValuesChangedListener(action: (spd: Int, rot: Int) -> Unit){ onCarValuesChanged = action }
    fun setOnCarValuesSetListener(action: (spd: Int, rot: Int) -> Unit){ onCarValuesSet = action }

    fun initiateFirstCalibration(tiltPortrait: Float, tiltLandscape: Float){
        //Only initialize calibration process if it is first time
        if(!isInitiallyCalibrated){
            isInitiallyCalibrated = true
            recalibrate(tiltPortrait, tiltLandscape)
        }
    }

    fun recalibrate(tiltPortrait: Float, tiltLandscape: Float){
        tiltCalibrationAngles[0] = tiltPortrait
        tiltCalibrationAngles[1] = tiltLandscape
    }

    fun setTiltValues(tiltPortrait: Float, tiltLandscape: Float){
        //Append to smoothing buffer + processing smoothing
        tiltPortraitBuffer.add(tiltPortrait)
        tiltLandscapeBuffer.add(tiltLandscape)

        val tiltPortAverage = tiltPortraitBuffer.average()
        if((tiltPortrait - tiltPortAverage).absoluteValue > smoothingRadThreshold){
            tiltAngles[0] = tiltPortAverage
        }

        val tiltLandAverage = tiltLandscapeBuffer.average()
        if((tiltLandscape - tiltLandAverage).absoluteValue > smoothingRadThreshold){
            tiltAngles[1] = tiltLandAverage
        }

        Log.d(TAG, "TiltProtAverage = ")

        processValues()
    }

    private fun processValues(){
        //Value to send
        val newSpeed = (speedPercentage * 5).toInt()
        val newRotation = (rotationPercentage * 5).toInt()

        onCarValuesSet(newSpeed, newRotation)
        if(newSpeed != speed || newRotation != rotation){
            speed = newSpeed
            rotation = newRotation
            onCarValuesChanged(speed, rotation)
        }

    }
    //endregion


}