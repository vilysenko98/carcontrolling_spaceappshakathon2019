package com.vilysenko98.spaceappshakathon2019.carcontrolling.views

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.view.View
import com.vilysenko98.spaceappshakathon2019.carcontrolling.R
import com.vilysenko98.spaceappshakathon2019.carcontrolling.utils.interpolateColor
import kotlin.math.absoluteValue

class CarValuesView(context: Context, attrs: AttributeSet): View(context, attrs) {
    //region Variables
    //Color of line
    var minColor = Color.GREEN
    var maxColor = Color.RED
    //Attributes
    var maxSpeed: Int = 5
        set(value) {
            field = value
            invalidate()
            requestLayout()
        }
    var maxRotation: Int = 5
        set(value) {
            field = value
            invalidate()
            requestLayout()
        }
    var currentSpeed: Int = 0
        set(value) {
            field = value
            invalidate()
            requestLayout()
        }
    var currentRotation: Int = 0
        set(value) {
            field = value
            invalidate()
            requestLayout()
        }
    //Paint
    private val paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.STROKE
        color = Color.BLACK
        strokeWidth = 18f
        strokeCap = Paint.Cap.ROUND
    }

    private val ovalRotationRect = RectF()

    //Other calculation values
    private val radius: Float
        get() = height / 2f - paint.strokeWidth / 2

    //endregion
    //Initialization
    init {
        context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.CarValuesView,
            0, 0).apply {

            try {
                maxSpeed = getInteger(R.styleable.CarValuesView_maxSpeed, 5)
                maxRotation = getInteger(R.styleable.CarValuesView_maxRotation, 5)
                currentSpeed = getInteger(R.styleable.CarValuesView_currentSpeed, 0)
                currentRotation = getInteger(R.styleable.CarValuesView_currentRotation, 0)
            } finally {
                recycle()
            }
        }
        invalidate()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        canvas.apply {
            val center = floatArrayOf(width / 2f, height / 2f)

            val speedPercent = currentSpeed / maxSpeed.toFloat()
            val radiusSpeedLineLength = -speedPercent * radius
            val radiusSpeedLine = center[1] + radiusSpeedLineLength

            val rotationPercent = currentRotation / maxRotation.toFloat()
            val rotationLine = center[1] + -rotationPercent * radius

            ovalRotationRect.set(center[0] - radiusSpeedLineLength.absoluteValue, center[1] - radiusSpeedLineLength.absoluteValue,
                center[0] + radiusSpeedLineLength.absoluteValue, center[1] + radiusSpeedLineLength.absoluteValue)

            paint.color = Color.GRAY
            drawCircle(center[0], center[1], radius, paint)
            paint.color = interpolateColor(minColor, maxColor, speedPercent.absoluteValue)
            drawLine(center[0], center[1], center[0], radiusSpeedLine, paint)

            paint.color = interpolateColor(minColor, maxColor, rotationPercent.absoluteValue)
            val startAngle: Float
            val sweepAngle: Float

            if(speedPercent.absoluteValue > 0) {
                if(speedPercent > 0) {
                    startAngle = -90f
                    sweepAngle = 90 * -rotationPercent
                } else {
                    startAngle = 90f
                    sweepAngle = 90 * rotationPercent
                }

                drawArc(
                    ovalRotationRect,
                    startAngle, sweepAngle,
                    false, paint
                )
            } else {
                drawLine(center[0], center[1], rotationLine, center[1], paint)
            }
        }
    }
    //endregion

}