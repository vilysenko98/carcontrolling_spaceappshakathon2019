package com.vilysenko98.spaceappshakathon2019.carcontrolling.utils

import com.vilysenko98.spaceappshakathon2019.carcontrolling.App

val PREFS_BT_ADDRESS = "com.vilysenko98.spaceappshakathon2019.carcontrolling.BT_ADDRESS"
val PREFS_WEBCAM_ADDRESS = "com.vilysenko98.spaceappshakathon2019.carcontrolling.WEBCAM_ADDRESS"
val PREFS_DEADZONE = "com.vilysenko98.spaceappshakathon2019.carcontrolling.DEADZONE"
val PREFS_MAX_ANGLE = "com.vilysenko98.spaceappshakathon2019.carcontrolling.MAX_ANGLE"
val PREFS_IS_CONTROLS_INVERTED = "com.vilysenko98.spaceappshakathon2019.carcontrolling.IS_CONTROLS_INVERTED"

var bluetoothDeviceAddress: String
    get() = App.appPrefs.getString(PREFS_BT_ADDRESS, "FF:FF:FF:FF:FF:FF")!!
    set(value) { App.appPrefs.edit().putString(PREFS_BT_ADDRESS, value.toUpperCase()).apply() }

var webcamWebAddress: String
    get() = App.appPrefs.getString(PREFS_WEBCAM_ADDRESS, "http://example.com")!!
    set(value) { App.appPrefs.edit().putString(PREFS_WEBCAM_ADDRESS, value).apply() }

var deadzoneAngle: Double
    get() = App.appPrefs.getFloat(PREFS_DEADZONE, Math.toRadians(5.0).toFloat()).toDouble()
    set(value) { App.appPrefs.edit().putFloat(PREFS_DEADZONE, value.toFloat()).apply() }

var maximumAngle: Double
    get() = App.appPrefs.getFloat(PREFS_MAX_ANGLE, Math.toRadians(25.0).toFloat()).toDouble()
    set(value) { App.appPrefs.edit().putFloat(PREFS_MAX_ANGLE, value.toFloat()).apply() }

var isControlsInverted: Boolean
    get() = App.appPrefs.getBoolean(PREFS_IS_CONTROLS_INVERTED, false)
    set(value) { App.appPrefs.edit().putBoolean(PREFS_IS_CONTROLS_INVERTED, value).apply() }