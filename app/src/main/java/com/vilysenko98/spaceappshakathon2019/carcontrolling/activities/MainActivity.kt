package com.vilysenko98.spaceappshakathon2019.carcontrolling.activities

import android.bluetooth.BluetoothAdapter
import android.content.Context
import android.content.Intent
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.webkit.WebViewClient
import android.widget.Toast
import com.github.douglasjunior.bluetoothclassiclibrary.BluetoothClassicService
import com.github.douglasjunior.bluetoothclassiclibrary.BluetoothConfiguration
import com.github.douglasjunior.bluetoothclassiclibrary.BluetoothService
import com.github.douglasjunior.bluetoothclassiclibrary.BluetoothStatus
import com.vilysenko98.spaceappshakathon2019.carcontrolling.App
import com.vilysenko98.spaceappshakathon2019.carcontrolling.R
import com.vilysenko98.spaceappshakathon2019.carcontrolling.car.CarValuesProcessor
import com.vilysenko98.spaceappshakathon2019.carcontrolling.utils.*
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*


class MainActivity : AppCompatActivity(), SensorEventListener {

    //region Variables
    val TAG = "CarControlling_TAG"
    lateinit var sensorManger: SensorManager
    lateinit var btService: BluetoothService

    //Sensor readings
    private val valuesMagnet = FloatArray(3)
    private val valuesAccel = FloatArray(3)
    private val valuesOrientation = FloatArray(3)
    private val rotationMatrix = FloatArray(9)

    val carValuesProcessor = CarValuesProcessor()

    //Bluetooth
    //To keep bluetooth connection and not let it time out
    var sendCounter = 0
    var maxIgnore = 10

    val bluetoothServiceUUID = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb")
    val btEventCallback = object : BluetoothService.OnBluetoothEventCallback {
        override fun onStatusChange(status: BluetoothStatus?) {
            when(status!!){
                BluetoothStatus.CONNECTED -> {
                    Toast.makeText(this@MainActivity, "Connected", Toast.LENGTH_SHORT).show()
                }
            }
        }

        override fun onToast(message: String?){
            Toast.makeText(this@MainActivity, message, Toast.LENGTH_SHORT).show()
        }

        override fun onDataRead(buffer: ByteArray?, length: Int) = Unit
        override fun onDataWrite(buffer: ByteArray?) = Unit

        override fun onDeviceName(deviceName: String?) = Unit
    }

    //endregion


    //region Android overrides
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
    }

    override fun onResume() {
        super.onResume()
        //Register sensor reading
        val delay = SensorManager.SENSOR_DELAY_GAME
        sensorManger.registerListener(this, sensorManger.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
            delay)
        sensorManger.registerListener(this, sensorManger.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD),
            delay)
    }

    override fun onPause() {
        super.onPause()
        //Pause sensor reading
        sensorManger.unregisterListener(this)
    }

    //endregion

    //region Sensor event listener implementations
    override fun onAccuracyChanged(sensor: Sensor, accuracy: Int) = Unit

    override fun onSensorChanged(event: SensorEvent) {
        //Obtain readings
        when (event.sensor.type) {
            Sensor.TYPE_ACCELEROMETER -> System.arraycopy(event.values, 0, valuesAccel, 0, valuesAccel.size)
            Sensor.TYPE_MAGNETIC_FIELD -> System.arraycopy(event.values, 0, valuesMagnet, 0, valuesMagnet.size)
        }

        SensorManager.getRotationMatrix(rotationMatrix, null, valuesAccel, valuesMagnet)
        SensorManager.getOrientation(rotationMatrix, valuesOrientation)

        //If values are pure zeros orientations are not obtained yet, so checking opposites
        if(!(valuesMagnet.isValuesSame(0f) && valuesAccel.isValuesSame(0f))){
            //Right now we uses only tilt angles which are last 2 elements

            with(carValuesProcessor) {
                //Initiate calibration(everytime but will work only once)
                initiateFirstCalibration(valuesOrientation[1], valuesOrientation[2])

                setTiltValues(valuesOrientation[1], valuesOrientation[2])
            }

            val degreeValues = valuesOrientation.radToDeg()
            runOnUiThread { updateDebugTexts(degreeValues[1], degreeValues[2]) }
        }
    }
    //endregion

    //region Custom methods
    private fun init(){
        //Bluetooth setup
        val config = BluetoothConfiguration().apply {
            context = applicationContext
            bluetoothServiceClass = BluetoothClassicService::class.java
            bufferSize = 1024
            characterDelimiter = '\n'
            callListenersInMainThread = true
            uuid = bluetoothServiceUUID
        }
        BluetoothService.init(config)
        btService = BluetoothService.getDefaultInstance().apply {
            setOnEventCallback(btEventCallback)
        }

        //Sensor setup
        sensorManger = getSystemService(Context.SENSOR_SERVICE) as SensorManager

        //Car processor setup
        carValuesProcessor.setOnCarValuesChangedListener { speed, rotation ->
            //Send values to device
            sendData(speed, rotation)
        }
        carValuesProcessor.setOnCarValuesSetListener { speed, rotation ->

            //Send invalid values to keep bluetooth connectivity
            sendCounter++
            if(sendCounter > maxIgnore) {
                sendCounter = 0
                sendData(127, 127)
            }

            //Update display view
            with(view_carValues){
                currentSpeed = (carValuesProcessor.speedPercentage * maxSpeed).toInt()
                currentRotation = (carValuesProcessor.rotationPercentage * maxRotation).toInt()
            }
            text_carValues.text = "speed: $speed\nrotation: $rotation"
        }

        //Setup views
        layout_debugInfo.visibility = if(App.isDebugging) View.VISIBLE else View.INVISIBLE

        with(web_camera){
            isHorizontalScrollBarEnabled = false
            isVerticalScrollBarEnabled = false
            isScrollbarFadingEnabled = false
            webViewClient = WebViewClient()
            settings.allowFileAccess =  true
            settings.javaScriptEnabled = true
            settings.javaScriptCanOpenWindowsAutomatically = true
        }
    }

    fun sendData(speed: Int, rotation: Int){
        when(btService.status){
            BluetoothStatus.CONNECTED -> {
                btService.write(byteArrayOf(
                    (speed * if(isControlsInverted) -1 else 1).toByte(),
                    (rotation * if(isControlsInverted) -1 else 1).toByte(),
                    '\n'.toByte()))
            }
        }
    }

    private fun updateDebugTexts(tiltPortrait: Float, tiltLandscape: Float){
        text_tiltPortrait.text = "Portrait tilt = ${tiltPortrait.toInt()}°"
        text_tiltLandscape.text = "Landscape tilt = ${tiltLandscape.toInt()}°"
    }
    //endregion

    //region Button listeners
    fun clickSettings(v: View){
        startActivity(Intent(this, SettingsActivity::class.java))
    }

    fun clickConnect(v: View){
        //Connect to bluetooth
        with(BluetoothAdapter.getDefaultAdapter()){
            if(isEnabled){
                btService.connect(getRemoteDevice(bluetoothDeviceAddress))
            } else {
                Toast.makeText(this@MainActivity, "Turn bluetooth on", Toast.LENGTH_SHORT).show()
            }
        }
        //Connect to webcam
        web_camera.loadUrl(webcamWebAddress)
    }

    fun clickRecalibrate(v: View){
        carValuesProcessor.recalibrate(valuesOrientation[1], valuesOrientation[2])
    }
    //endregion
}
