package com.vilysenko98.spaceappshakathon2019.carcontrolling

import android.app.Application
import android.content.SharedPreferences
import android.preference.PreferenceManager

class App: Application() {
    companion object{
        val isDebugging = false
        lateinit var appPrefs : SharedPreferences
    }

    override fun onCreate() {
        super.onCreate()
        appPrefs = PreferenceManager.getDefaultSharedPreferences(this)
    }
}